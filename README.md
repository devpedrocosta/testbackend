<p align="center">
  <a href="https://www.linkedin.com/in/pedro-costa-b5607697/" target="blank"><img src="https://media-exp1.licdn.com/dms/image/C4D03AQEgaHX_6q3eKA/profile-displayphoto-shrink_200_200/0?e=1601510400&v=beta&t=vJQ73wCFDjbOqDZizLNuOSiK6csoxB_Z9IB6cxLhg6k" width="320" alt="Nest Logo" /></a>
</p>



## Desafio

Requisitos back-end: 
Rota para cadastrar um novo revendedor(a) exigindo no mínimo nome completo, CPF, e- mail e senha; 
Rota para validar um login de um revendedor(a); 
Rota para cadastrar uma nova compra exigindo no mínimo código, valor, data e CPF do 
revendedor(a). Todos os cadastros são salvos com o status “Em validação” exceto quando o CPF do revendedor(a) for 153.509.460-56, neste caso o status é salvo como “Aprovado”; 
Rota para listar as compras cadastradas retornando código, valor, data, % de cashback aplicado para esta compra, valor de cashback para esta compra e status; 
Rota para exibir o acumulado de cashback até o momento, essa rota irá consumir essa informação de uma API externa disponibilizada pelo Boticário. 
API externa GET: https://mdaqk8ek5j.execute-api.us-east-1.amazonaws.com/v1/cashback?cpf=12312312323 
headers { token: 'ZXPURQOARHiMc6Y0flhRC1LVlZQVFRnm' } 


## Instalacao

```bash
$ npm install
```

## Comandos

```bash
# development
$ npm run start

```

## Documentacao

```bash
# Para poder visualizar todos  endpoinsts
$ http://localhost:3000/api/#/

```

## Test

```bash

# e2e tests
$ npm run test:e2e

# unit tests
$ npm run test

```

## Roadmap

- A api esta usando um servico de banco de dados externo mongo no https://mlab.com/ 
-  crie um novo usuario 
- faca o login para que seja gerado o token e poder acessar os outros end-points usando o bearer token.

## Endpoints

Servidor port : 3000

- (POST)cadastro revendedor : /revendedor/create
- (POST)login revendedor : /auth/login
- (POST)cadastro de compra : /revendedor/buy (Autenticação JWT)
- (GET)buscar todas compras : /revendedor/buy/cashback (Autenticação JWT)
- (GET)busca credito em api externa cpf obrigatorio : /revendedor/accumulated/cashback/:cpf


# ScreanShot
 - Na pasta screanshot tem algumas evidencias de teste do uso dos Endpoints da api