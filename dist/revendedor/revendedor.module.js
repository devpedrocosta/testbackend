"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RevendedorModule = void 0;
const common_1 = require("@nestjs/common");
const revendedor_controller_1 = require("./revendedor.controller");
const revendedor_service_1 = require("./revendedor.service");
const revendedor_provider_1 = require("./providers/revendedor.provider");
const database_module_1 = require("./../Modules/database/database.module");
const buy_provider_1 = require("./providers/buy.provider");
const passport_1 = require("@nestjs/passport");
let RevendedorModule = class RevendedorModule {
};
RevendedorModule = __decorate([
    common_1.Module({
        imports: [
            passport_1.PassportModule.register({ defaultStrategy: 'jwt', session: false }),
            database_module_1.DatabaseModule,
            common_1.HttpModule,
        ],
        exports: [revendedor_service_1.RevendedorService],
        controllers: [revendedor_controller_1.RevendedorController],
        providers: [revendedor_service_1.RevendedorService, ...revendedor_provider_1.revendedorProviders, ...buy_provider_1.buyProviders]
    })
], RevendedorModule);
exports.RevendedorModule = RevendedorModule;
//# sourceMappingURL=revendedor.module.js.map