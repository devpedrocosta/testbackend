import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
export declare const BuySchema: mongoose.Schema<any>;
export interface BuyDOC extends Document {
    codigo: string;
    cpf: string;
    valor: number;
    data: Date;
    status: string;
    cashback: number;
    cashbackValue: number;
}
