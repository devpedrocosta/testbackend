"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RevendedorSchema = void 0;
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
exports.RevendedorSchema = new mongoose.Schema({
    fullnome: String,
    senha: {
        type: String,
        required: true
    },
    email: { type: String, required: true, index: { unique: true } },
    cpf: { type: String, index: { unique: true } },
});
exports.RevendedorSchema.methods.checkPassword = function (attempt, callback) {
    let user = this;
    bcrypt.compare(attempt, user.password, (err, isMatch) => {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};
//# sourceMappingURL=revendedor.schema.js.map