"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuySchema = void 0;
const mongoose = require("mongoose");
exports.BuySchema = new mongoose.Schema({
    codigo: { type: String, index: { unique: true } },
    cpf: String,
    valor: { type: Number, default: 0 },
    data: { type: Date, default: Date.now },
    cashback: Number,
    cashbackValue: Number,
    status: { type: String, default: 'Em validação' }
});
//# sourceMappingURL=buy.shema.js.map