import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
export declare const RevendedorSchema: mongoose.Schema<any>;
export interface RevendedorDOC extends Document {
    fullnome: string;
    senha: string;
    email: string;
    cpf: string;
}
