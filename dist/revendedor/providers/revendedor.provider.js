"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.revendedorProviders = void 0;
const revendedor_schema_1 = require("../schema/revendedor.schema");
exports.revendedorProviders = [
    {
        provide: 'REV_MODEL',
        useFactory: (connection) => connection.model('Revendedor', revendedor_schema_1.RevendedorSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=revendedor.provider.js.map