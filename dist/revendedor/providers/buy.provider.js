"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buyProviders = void 0;
const buy_shema_1 = require("../schema/buy.shema");
exports.buyProviders = [
    {
        provide: 'BUY_MODEL',
        useFactory: (connection) => connection.model('Buy', buy_shema_1.BuySchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=buy.provider.js.map