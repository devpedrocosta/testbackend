"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RevendedorController = void 0;
const common_1 = require("@nestjs/common");
const revendedor_class_1 = require("./dto/revendedor.class");
const cpf_transform_pipe_1 = require("./pipe/cpf.transform.pipe");
const revendedor_service_1 = require("./revendedor.service");
const buy_class_1 = require("./dto/buy.class");
const aproved_pipe_1 = require("./pipe/aproved.pipe");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
let RevendedorController = class RevendedorController {
    constructor(revendedor) {
        this.revendedor = revendedor;
    }
    async createRevendedor(revendedorDto) {
        return await this.revendedor.register(revendedorDto);
    }
    async createRevendedorBuy(buyProduct) {
        return await this.revendedor.registerBuy(buyProduct);
    }
    async getcashback() {
        return this.revendedor.findAllBuys();
    }
    async getRevendedorcashback(params) {
        return this.revendedor.findAccumulatedCashback(params.cpf).toPromise();
    }
};
__decorate([
    common_1.Post('create'),
    swagger_1.ApiOperation({ summary: 'criar novos revendedores' }),
    __param(0, common_1.Body((new cpf_transform_pipe_1.ValidationCpfPipe()))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [revendedor_class_1.RevendedorDTO]),
    __metadata("design:returntype", Promise)
], RevendedorController.prototype, "createRevendedor", null);
__decorate([
    common_1.Post('buy'),
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiOperation({ summary: 'criar nova compra do revendedor' }),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Body((new aproved_pipe_1.ValidationAprovedPipe()))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [buy_class_1.BuyDTO]),
    __metadata("design:returntype", Promise)
], RevendedorController.prototype, "createRevendedorBuy", null);
__decorate([
    common_1.Get('buy/cashback'),
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiOperation({ summary: 'buscar todas compras dos revendedores' }),
    common_1.UseGuards(passport_1.AuthGuard()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], RevendedorController.prototype, "getcashback", null);
__decorate([
    common_1.Get('accumulated/cashback/:cpf'),
    swagger_1.ApiOperation({ summary: 'buscar credito api externa ' }),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RevendedorController.prototype, "getRevendedorcashback", null);
RevendedorController = __decorate([
    swagger_1.ApiTags('Revendedor'),
    common_1.Controller('revendedor'),
    __metadata("design:paramtypes", [revendedor_service_1.RevendedorService])
], RevendedorController);
exports.RevendedorController = RevendedorController;
//# sourceMappingURL=revendedor.controller.js.map