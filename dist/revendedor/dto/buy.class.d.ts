export declare class BuyDTO {
    codigo: string;
    cpf: string;
    valor: number;
    data: Date;
}
