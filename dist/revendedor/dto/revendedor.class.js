"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RevendedorDTO = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class RevendedorDTO {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiProperty({ example: 'pedro joaquim da costa', description: 'nome completo' }),
    __metadata("design:type", String)
], RevendedorDTO.prototype, "fullnome", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiProperty({ example: '234.566.345-69', description: 'cpf ' }),
    __metadata("design:type", String)
], RevendedorDTO.prototype, "cpf", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiProperty({ example: '*********', description: 'senha ' }),
    __metadata("design:type", String)
], RevendedorDTO.prototype, "senha", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEmail(),
    swagger_1.ApiProperty({ example: 'pedro@pedro.com', description: 'email' }),
    __metadata("design:type", String)
], RevendedorDTO.prototype, "email", void 0);
exports.RevendedorDTO = RevendedorDTO;
//# sourceMappingURL=revendedor.class.js.map