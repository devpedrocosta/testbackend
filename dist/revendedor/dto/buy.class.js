"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyDTO = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class BuyDTO {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsInt(),
    swagger_1.ApiProperty({ example: '123', description: 'codigo da compra ' }),
    __metadata("design:type", String)
], BuyDTO.prototype, "codigo", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    swagger_1.ApiProperty({ example: '235.567.345-69', description: 'cpf do revendedor ' }),
    __metadata("design:type", String)
], BuyDTO.prototype, "cpf", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsDecimal(),
    swagger_1.ApiProperty({ example: '1500', description: 'valor da compra ' }),
    __metadata("design:type", Number)
], BuyDTO.prototype, "valor", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsDate(),
    swagger_1.ApiProperty({ example: '2020-08-01T10:59:00Z', description: 'codigo da compra ' }),
    __metadata("design:type", Date)
], BuyDTO.prototype, "data", void 0);
exports.BuyDTO = BuyDTO;
//# sourceMappingURL=buy.class.js.map