export declare class RevendedorDTO {
    fullnome: string;
    cpf: string;
    senha: string;
    email: string;
}
export interface Revendedor {
    fullnome: string;
    cpf: string;
    email: string;
}
