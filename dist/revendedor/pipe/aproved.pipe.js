"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationAprovedPipe = void 0;
const common_1 = require("@nestjs/common");
let ValidationAprovedPipe = class ValidationAprovedPipe {
    async transform(value) {
        const data = value;
        if (this.validCpf(value.cpf)) {
            data.status = "Aprovado";
        }
        return value;
    }
    validCpf(cpf) {
        return cpf === "153.509.460-56" || cpf === "15350946056" ? true : false;
    }
};
ValidationAprovedPipe = __decorate([
    common_1.Injectable()
], ValidationAprovedPipe);
exports.ValidationAprovedPipe = ValidationAprovedPipe;
//# sourceMappingURL=aproved.pipe.js.map