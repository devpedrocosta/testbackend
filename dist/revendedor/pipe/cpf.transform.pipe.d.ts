import { PipeTransform } from "@nestjs/common";
export declare class ValidationCpfPipe implements PipeTransform<any> {
    transform(value: any): Promise<any>;
}
