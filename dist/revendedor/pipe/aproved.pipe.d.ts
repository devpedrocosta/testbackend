import { PipeTransform } from "@nestjs/common";
export declare class ValidationAprovedPipe implements PipeTransform<any> {
    transform(value: any): Promise<any>;
    validCpf(cpf: string): boolean;
}
