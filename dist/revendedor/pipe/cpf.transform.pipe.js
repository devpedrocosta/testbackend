"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationCpfPipe = void 0;
const common_1 = require("@nestjs/common");
const cpf_1 = require("./../../Modules/shared/cpf");
let ValidationCpfPipe = class ValidationCpfPipe {
    async transform(value) {
        const isCPFValid = cpf_1.cpf(value.cpf);
        if (!isCPFValid) {
            throw new common_1.BadRequestException("CPF invalido");
        }
        return value;
    }
};
ValidationCpfPipe = __decorate([
    common_1.Injectable()
], ValidationCpfPipe);
exports.ValidationCpfPipe = ValidationCpfPipe;
//# sourceMappingURL=cpf.transform.pipe.js.map