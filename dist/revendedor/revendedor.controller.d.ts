import { RevendedorDTO } from './dto/revendedor.class';
import { RevendedorService } from './revendedor.service';
import { BuyDTO } from './dto/buy.class';
export declare class RevendedorController {
    private revendedor;
    constructor(revendedor: RevendedorService);
    createRevendedor(revendedorDto: RevendedorDTO): Promise<import("../Modules/shared/RegistrationStatus").RegistrationStatus>;
    createRevendedorBuy(buyProduct: BuyDTO): Promise<import("../Modules/shared/RegistrationStatus").RegistrationStatus>;
    getcashback(): Promise<import("../Modules/shared/RegistrationStatus").RegistrationStatus>;
    getRevendedorcashback(params: any): Promise<{
        success: boolean;
        message: string;
        data: any;
    }>;
}
