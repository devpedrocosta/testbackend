"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RevendedorService = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = require("bcrypt");
const mongoose_1 = require("mongoose");
const operators_1 = require("rxjs/operators");
const RegistrationStatus_1 = require("../Modules/shared/RegistrationStatus");
let RevendedorService = class RevendedorService {
    constructor(httpService, revendedorModel, buyModel) {
        this.httpService = httpService;
        this.revendedorModel = revendedorModel;
        this.buyModel = buyModel;
    }
    async register(revendedor) {
        try {
            const salt = await bcrypt.genSalt(10);
            revendedor.senha = await bcrypt.hash(revendedor.senha, salt);
            await new this.revendedorModel(revendedor).save();
            return {
                success: true,
                message: "Revendedor cadastrado",
                data: revendedor,
            };
        }
        catch (error) {
            throw new common_1.ConflictException("Revendedor ja esta cadastrado");
        }
    }
    async findByEmail(email) {
        return await this.revendedorModel.findOne({ email: email });
    }
    async findAllBuys() {
        try {
            const buys = await this.buyModel.find();
            return {
                success: true,
                message: "Todas compras",
                data: buys,
            };
        }
        catch (error) {
            throw new common_1.HttpException("Erro ao carregar todas compras", common_1.HttpStatus.NOT_FOUND);
        }
    }
    findAccumulatedCashback(cpf) {
        try {
            const headersRequest = {
                Authorization: `token ZXPURQOARHiMc6Y0flhRC1LVlZQVFRnm`,
            };
            return this.httpService
                .get(`https://mdaqk8ek5j.execute-api.us-east-1.amazonaws.com/v1/cashback?cpf=${cpf}`, { headers: headersRequest })
                .pipe(operators_1.map((body) => ({
                success: true,
                message: "Cashback",
                data: body.data.body,
            })));
        }
        catch (error) {
            throw new common_1.HttpException("Erro ao credit cashback", common_1.HttpStatus.NOT_FOUND);
        }
    }
    async registerBuy(buyProduct) {
        try {
            const cashback = this.cashbackCalculate(buyProduct.valor);
            const cashbackValue = (buyProduct.valor * cashback) / 100;
            const newBuy = Object.assign(Object.assign({}, buyProduct), { cashback, cashbackValue });
            await new this.buyModel(newBuy).save();
            return {
                success: true,
                message: "Compra cadastrada com sucesso",
            };
        }
        catch (error) {
            throw new common_1.HttpException("Erro ao cadastrar a compra", common_1.HttpStatus.BAD_REQUEST);
        }
    }
    cashbackCalculate(price) {
        if (price <= 1000)
            return 10;
        if (price > 1000 && price < 1500)
            return 15;
        if (price > 1500)
            return 20;
    }
};
RevendedorService = __decorate([
    common_1.Injectable(),
    __param(1, common_1.Inject("REV_MODEL")),
    __param(2, common_1.Inject("BUY_MODEL")),
    __metadata("design:paramtypes", [common_1.HttpService,
        mongoose_1.Model,
        mongoose_1.Model])
], RevendedorService);
exports.RevendedorService = RevendedorService;
//# sourceMappingURL=revendedor.service.js.map