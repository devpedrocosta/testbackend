import { HttpService } from "@nestjs/common";
import { RevendedorDTO } from "./dto/revendedor.class";
import { BuyDTO } from "./dto/buy.class";
import { Model } from "mongoose";
import { RevendedorDOC } from "./schema/revendedor.schema";
import { BuyDOC } from "./schema/buy.shema";
import { RegistrationStatus } from "src/Modules/shared/RegistrationStatus";
export declare class RevendedorService {
    private httpService;
    private revendedorModel;
    private buyModel;
    constructor(httpService: HttpService, revendedorModel: Model<RevendedorDOC>, buyModel: Model<BuyDOC>);
    register(revendedor: RevendedorDTO): Promise<RegistrationStatus>;
    findByEmail(email: any): Promise<RevendedorDOC>;
    findAllBuys(): Promise<RegistrationStatus>;
    findAccumulatedCashback(cpf: any): import("rxjs").Observable<{
        success: boolean;
        message: string;
        data: any;
    }>;
    registerBuy(buyProduct: BuyDTO): Promise<RegistrationStatus>;
    cashbackCalculate(price: number): 10 | 15 | 20;
}
