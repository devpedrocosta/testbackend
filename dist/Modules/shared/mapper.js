"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toRevendedor = void 0;
exports.toRevendedor = (data) => {
    const { fullnome, cpf, email } = data;
    return {
        fullnome,
        email,
        cpf,
    };
};
//# sourceMappingURL=mapper.js.map