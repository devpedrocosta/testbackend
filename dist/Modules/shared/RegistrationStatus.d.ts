export interface RegistrationStatus {
    success: boolean;
    message: string;
    data: any;
}
