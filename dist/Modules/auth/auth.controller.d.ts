import { AuthService } from './auth.service';
import { Login } from './interfaces/login';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    login(loginUser: Login): Promise<{
        expiresIn: number;
        token: string;
    }>;
}
