"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const bcrypt = require("bcrypt");
const revendedor_service_1 = require("./../../revendedor/revendedor.service");
let AuthService = class AuthService {
    constructor(usersService, jwtService) {
        this.usersService = usersService;
        this.jwtService = jwtService;
    }
    async validateUserByPassword(loginAttempt) {
        const userToAttempt = await this.usersService.findByEmail(loginAttempt.email);
        const isMatch = await bcrypt.compare(loginAttempt.senha, userToAttempt.senha);
        if (isMatch) {
            return this.createJwtPayload(userToAttempt);
        }
        else {
            throw new common_1.UnauthorizedException();
        }
    }
    async validateUserByJwt(payload) {
        let user = await this.usersService.findByEmail(payload.email);
        if (user) {
            return this.createJwtPayload(user);
        }
        else {
            throw new common_1.UnauthorizedException();
        }
    }
    createJwtPayload(user) {
        const data = {
            email: user.email,
        };
        const jwt = this.jwtService.sign(data);
        return {
            expiresIn: 3600,
            token: jwt,
        };
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [revendedor_service_1.RevendedorService,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map