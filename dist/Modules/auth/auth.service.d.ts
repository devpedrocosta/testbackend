import { JwtService } from "@nestjs/jwt";
import { JwtPayload } from "./interfaces/jwt-payload.interface";
import { RevendedorService } from "./../../revendedor/revendedor.service";
import { Login } from "./interfaces/login";
export declare class AuthService {
    private usersService;
    private jwtService;
    constructor(usersService: RevendedorService, jwtService: JwtService);
    validateUserByPassword(loginAttempt: Login): Promise<{
        expiresIn: number;
        token: string;
    }>;
    validateUserByJwt(payload: JwtPayload): Promise<{
        expiresIn: number;
        token: string;
    }>;
    createJwtPayload(user: any): {
        expiresIn: number;
        token: string;
    };
}
