import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import * as request from "supertest";
import { AppModule } from "./../src/app.module";

describe("Integration test (e2e)", () => {
  let app: INestApplication;
  const login = {
    email: "hoooo@pedro.com",
    senha: "pedro123",
  };

  const revendedor = {
    email: `hoooo@pedro.com${Math.random()
      .toString(36)
      .substring(7)}`,
    senha: "pedro123",
    fullnome: "test",
    cpf: `382.741.570-52`,
  };
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it("/ (GET)", () => {
    return request(app.getHttpServer())
      .get("/")
      .expect(200)
      .expect("Test boticario");
  });

  it("Test : external api cashback  with cpf ", () => {
    return request(app.getHttpServer())
      .get("/revendedor/accumulated/cashback/015.477.690-41")
      .expect(200);
  });

  it("Test : revendedor exst on database", () => {
     const dataSaved = revendedor ; 
      dataSaved.cpf = `444.583.250-27`;
    return request(app.getHttpServer())
      .post("/revendedor/create")
      .send(dataSaved)
      .expect(409);
  });

 
});
