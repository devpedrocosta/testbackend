import { Revendedor } from "./../../revendedor/dto/revendedor.class";

export const toRevendedor = (data: Revendedor): Revendedor => {
  const { fullnome, cpf, email } = data;

  return {
    fullnome,
    email,
    cpf,
  } as Revendedor;
};
