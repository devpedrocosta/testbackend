import { ApiProperty } from "@nestjs/swagger";

export class Login {
    @ApiProperty({ example: 'test@test.com', description: 'email do usuario ' })
    email: string;
  
    @ApiProperty({ example: '******', description: 'senha do login do usuuario' })
    senha: number;
  

  }