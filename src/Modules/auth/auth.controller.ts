import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Login } from './interfaces/login';
import { ApiBearerAuth, ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {

    }

    @Post('login') 
    @ApiOperation({ summary: 'Login' })
    async login(@Body() loginUser: Login){
        return await this.authService.validateUserByPassword(loginUser);
    }

}