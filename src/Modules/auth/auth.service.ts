import { Injectable, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from "bcrypt";
import { JwtPayload } from "./interfaces/jwt-payload.interface";
import { RevendedorService } from "./../../revendedor/revendedor.service";
import { Login } from "./interfaces/login";

@Injectable()
export class AuthService {
  constructor(
    private usersService: RevendedorService,
    private jwtService: JwtService
  ) {}

  async validateUserByPassword(loginAttempt: Login) {
    const userToAttempt = await this.usersService.findByEmail(
      loginAttempt.email
    );
    const isMatch = await bcrypt.compare(
      loginAttempt.senha,
      userToAttempt.senha
    );
    if (isMatch) {
      return this.createJwtPayload(userToAttempt);
    } else {
      throw new UnauthorizedException();
    }
  }

  async validateUserByJwt(payload: JwtPayload) {
    let user = await this.usersService.findByEmail(payload.email);

    if (user) {
      return this.createJwtPayload(user);
    } else {
      throw new UnauthorizedException();
    }
  }

  createJwtPayload(user) {
    const data: JwtPayload = {
      email: user.email,
    };

    const jwt = this.jwtService.sign(data);

    return {
      expiresIn: 3600,
      token: jwt,
    };
  }
}
