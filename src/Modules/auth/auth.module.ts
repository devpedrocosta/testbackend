import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './strategies/jwt.strategy';

import { PassportModule } from '@nestjs/passport';
import { RevendedorModule } from './../../revendedor/revendedor.module';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 'thisismykickasssecretthatiwilltotallychangelater',
      signOptions: {
        expiresIn: 3600
      }
    }),
    RevendedorModule
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy]
})
export class AuthModule {}