import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { RevendedorModule } from './revendedor/revendedor.module';
import { AuthModule } from './Modules/auth/auth.module';

@Module({
  imports: [ConfigModule.forRoot({
    isGlobal: true,
    envFilePath: '.development.env',
  }) ,AuthModule,RevendedorModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
