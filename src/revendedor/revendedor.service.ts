import {
  Injectable,
  HttpException,
  HttpStatus,
  Inject,
  ConflictException,
  HttpService,
} from "@nestjs/common";
import * as bcrypt from "bcrypt";
import { Revendedor, RevendedorDTO } from "./dto/revendedor.class";
import { BuyDTO } from "./dto/buy.class";
import { Model } from "mongoose";
import { RevendedorDOC } from "./schema/revendedor.schema";
import { BuyDOC } from "./schema/buy.shema";
import { map } from "rxjs/operators";
import { RegistrationStatus } from "src/Modules/shared/RegistrationStatus";

@Injectable()
export class RevendedorService {
  constructor(
    private httpService: HttpService,
    @Inject("REV_MODEL") private revendedorModel: Model<RevendedorDOC>,
    @Inject("BUY_MODEL") private buyModel: Model<BuyDOC>
  ) {}

  async register(revendedor: RevendedorDTO): Promise<RegistrationStatus> {
    try {
      const salt = await bcrypt.genSalt(10);
      revendedor.senha = await bcrypt.hash(revendedor.senha, salt);
      await new this.revendedorModel(revendedor).save();
      return {
        success: true,
        message: "Revendedor cadastrado",
        data: revendedor as Revendedor,
      } as RegistrationStatus;
    } catch (error) {
      throw new ConflictException("Revendedor ja esta cadastrado");
    }
  }

  async findByEmail(email) {
    return await this.revendedorModel.findOne({ email: email });
  }

  async findAllBuys(): Promise<RegistrationStatus> {
    try {
      const buys = await this.buyModel.find();
      return {
        success: true,
        message: "Todas compras",
        data: buys,
      } as RegistrationStatus;
    } catch (error) {
      throw new HttpException(
        "Erro ao carregar todas compras",
        HttpStatus.NOT_FOUND
      );
    }
  }

  findAccumulatedCashback(cpf) {
    try {
      const headersRequest = {
        Authorization: `token ZXPURQOARHiMc6Y0flhRC1LVlZQVFRnm`,
      };
      return this.httpService
        .get(
          `https://mdaqk8ek5j.execute-api.us-east-1.amazonaws.com/v1/cashback?cpf=${cpf}`,
          { headers: headersRequest }
        )
        .pipe(
          map((body) => ({
            success: true,
            message: "Cashback",
            data: body.data.body,
          }))
        );
    } catch (error) {
      throw new HttpException("Erro ao credit cashback", HttpStatus.NOT_FOUND);
    }
  }

  async registerBuy(buyProduct: BuyDTO): Promise<RegistrationStatus> {
    try {
      const cashback = this.cashbackCalculate(buyProduct.valor);
      const cashbackValue = (buyProduct.valor * cashback) / 100;
      const newBuy = { ...buyProduct, cashback, cashbackValue };
      await new this.buyModel(newBuy).save();
      return {
        success: true,
        message: "Compra cadastrada com sucesso",
      } as RegistrationStatus;
    } catch (error) {
      throw new HttpException(
        "Erro ao cadastrar a compra",
        HttpStatus.BAD_REQUEST
      );
    }
  }

  cashbackCalculate(price: number) {
    if (price <= 1000) return 10;
    if (price > 1000 && price < 1500) return 15;
    if (price > 1500) return 20;
  }
}
