import { Connection } from 'mongoose';
import { RevendedorSchema } from '../schema/revendedor.schema';

export const revendedorProviders = [
  {
    provide: 'REV_MODEL',
    useFactory: (connection: Connection) => connection.model('Revendedor', RevendedorSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];