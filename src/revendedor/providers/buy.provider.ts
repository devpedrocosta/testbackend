import { Connection } from 'mongoose';
import { BuySchema } from '../schema/buy.shema';

export const buyProviders = [
  {
    provide: 'BUY_MODEL',
    useFactory: (connection: Connection) => connection.model('Buy', BuySchema),
    inject: ['DATABASE_CONNECTION'],
  },
];