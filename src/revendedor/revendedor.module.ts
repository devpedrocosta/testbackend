import { Module, HttpModule } from '@nestjs/common';
import { RevendedorController } from './revendedor.controller';
import { RevendedorService } from './revendedor.service';
import { revendedorProviders } from './providers/revendedor.provider';
import { DatabaseModule } from './../Modules/database/database.module';
import { buyProviders } from './providers/buy.provider';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    DatabaseModule,
    HttpModule,
  ],
  exports:[RevendedorService],
  controllers: [RevendedorController],
  providers: [RevendedorService,...revendedorProviders,...buyProviders]
})
export class RevendedorModule {}
