import { Controller, Post, Body, Get, UseGuards, Req, Param } from '@nestjs/common';
import { RevendedorDTO } from './dto/revendedor.class';
import {  ValidationCpfPipe } from './pipe/cpf.transform.pipe';
import { RevendedorService } from './revendedor.service';
import { BuyDTO } from './dto/buy.class';
import { ValidationAprovedPipe } from './pipe/aproved.pipe';
import { AuthGuard } from '@nestjs/passport';
import { ApiOperation, ApiResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';


@ApiTags('Revendedor')
@Controller('revendedor')
export class RevendedorController {

    constructor(private revendedor:RevendedorService){
        
    }

    @Post('create')
    @ApiOperation({ summary: 'criar novos revendedores' })
    public async createRevendedor(@Body((new ValidationCpfPipe())) revendedorDto: RevendedorDTO) {
      return await this.revendedor.register(revendedorDto);
    }
 
    @Post('buy')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'criar nova compra do revendedor' })
    @UseGuards(AuthGuard())
    public async createRevendedorBuy(@Body((new ValidationAprovedPipe())) buyProduct: BuyDTO) {
      return await this.revendedor.registerBuy(buyProduct);
    }
    
    @Get('buy/cashback')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'buscar todas compras dos revendedores' })
    @UseGuards(AuthGuard())
    public async getcashback()  {
      return this.revendedor.findAllBuys();
    }
 
    @Get('accumulated/cashback/:cpf')
    @ApiOperation({ summary: 'buscar credito api externa ' })
    public async getRevendedorcashback(@Param() params)  {
      return this.revendedor.findAccumulatedCashback(params.cpf).toPromise();
    }
}
