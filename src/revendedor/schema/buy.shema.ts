import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
export const BuySchema = new mongoose.Schema({
    codigo: {type: String, index: {unique: true}},
    cpf: String, 
    valor:{ type: Number, default: 0 } ,
    data: { type: Date, default: Date.now },
    cashback:Number,
    cashbackValue:Number,
    status:{ type: String, default: 'Em validação'}
});

export interface BuyDOC extends Document {
    codigo: string;
    cpf: string; 
    valor:number; 
    data: Date;
    status:string;
    cashback:number;
    cashbackValue:number;
}

