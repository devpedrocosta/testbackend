import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import * as bcrypt from 'bcrypt';
export const RevendedorSchema = new mongoose.Schema({
  fullnome: String,
  senha: {
    type: String,
    required: true
},
  email:{type: String, required: true,index: {unique: true}},
  cpf: {type: String, index: {unique: true}},
});



RevendedorSchema.methods.checkPassword = function(attempt, callback){

  let user = this;

  bcrypt.compare(attempt, user.password, (err, isMatch) => {
      if(err) return callback(err);
      callback(null, isMatch);
  });

};
export interface RevendedorDOC extends Document {
    fullnome: string,
    senha: string,
    email: string,
    cpf: string,
}