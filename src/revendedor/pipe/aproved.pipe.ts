import { PipeTransform, Injectable } from "@nestjs/common";

@Injectable()
export class ValidationAprovedPipe implements PipeTransform<any> {
  async transform(value: any) {
    const data = value;
    if (this.validCpf(value.cpf)) {
      data.status = "Aprovado";
    }
    return value;
  }
  // aqui poderia ser feito uma requisicao para um banco  que retorna uma lista tornando mais dinamico
  validCpf(cpf: string) {
    return cpf === "153.509.460-56" || cpf === "15350946056" ? true : false;
  }
}
