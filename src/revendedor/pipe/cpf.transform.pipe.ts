import {
  PipeTransform,
  Injectable,
  BadRequestException,
} from "@nestjs/common";

import { cpf } from "./../../Modules/shared/cpf";

@Injectable()
export class ValidationCpfPipe implements PipeTransform<any> {
  async transform(value: any) {
    const isCPFValid = cpf(value.cpf);

    if (!isCPFValid) {
      throw new BadRequestException("CPF invalido");
    }
    return value;
  }
}
