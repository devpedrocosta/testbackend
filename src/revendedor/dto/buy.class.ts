import { IsNotEmpty,IsInt, IsString, IsDecimal, IsDate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class BuyDTO {
 
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ example: '123', description: 'codigo da compra ' })
  codigo: string;

  
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: '235.567.345-69', description: 'cpf do revendedor ' })
  cpf: string;

  @IsNotEmpty()
  @IsDecimal()
  @ApiProperty({ example: '1500', description: 'valor da compra ' })
  valor:number; 

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({ example: '2020-08-01T10:59:00Z', description: 'codigo da compra ' })
  data: Date;
}
