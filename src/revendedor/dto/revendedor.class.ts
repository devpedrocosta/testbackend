import { IsNotEmpty, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RevendedorDTO {
  @IsNotEmpty()
  @ApiProperty({ example: 'pedro joaquim da costa', description: 'nome completo' })
  fullnome: string;

  @IsNotEmpty()
  @ApiProperty({ example: '234.566.345-69', description: 'cpf ' })
  cpf: string;

  @IsNotEmpty()
  @ApiProperty({ example: '*********', description: 'senha ' })
  senha: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ example: 'pedro@pedro.com', description: 'email' })
  email: string;
}


export interface Revendedor {

  fullnome: string;

  cpf: string;

  email: string;

  
}
